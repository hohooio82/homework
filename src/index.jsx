import ForgeUI, { render, Fragment, IssuePanel, Form, TextField, useState,useEffect } from '@forge/ui';
import { storage } from '@forge/api';

const App = () => {
  const [formState, setFormState] = useState('');
  const onSubmit = async (formData) => {
    console.log(formData)
    setFormState(formData['text-input']);
    storage.set('text-input',formData['text-input'])
  };
  useEffect(async ()=>{
    const text = await storage.get('text-input');
    text!==undefined?setFormState(text):null;
  },[])
  return (
    <Fragment>
      <Form onSubmit={onSubmit}>
        <TextField name="text-input" label="text-input" defaultValue={formState} />
      </Form>
    </Fragment>
  );
};

export const run = render(
  <IssuePanel>
    <App />
  </IssuePanel>
);
